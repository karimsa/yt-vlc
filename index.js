require('colors')

var http = require('http')
var pump = require('pump')
var search = require('youtube-search')
var ytdl = require('youtube-dl')
var proc = require('child_process')
var querystring = require('querystring')

var url = process.argv[2]
var server = http.createServer()
var isNumber = function (strN) { return !Number.isNaN(parseInt(strN, 10)); }

if (!url) throw 'please specify a url/option';

if (url === 'search') {
	if (!process.argv[3]) throw 'please try to search for something';

	var args = process.argv.slice(3).join(' ')
	var start_i = 1

	if (isNumber(process.argv[3])) {
		args = process.argv.slice(4).join(' ')
		start_i = parseInt(process.argv[3], 10)
	}

	search(args, {
		maxResults: 15,
		startIndex: start_i
	}, function (err, results) {
		if (err) {
			console.error(err)
			process.exit(-1)
		}

		results.forEach(function (vid, i) {
			var qs = querystring.parse(vid.url.substr(vid.url.indexOf('?') + 1))
			console.log ( '  [%s] %s - %s', String(i + 1).red.bold, String(vid.title).yellow.bold, qs.v.blue.bold )
		})
	})
} else {

	var go = function () {

		var id = url;

		if (url.indexOf('://') === -1) url = 'http://www.youtube.com/watch?v=' + url;

		//console.log('    NOW PLAYING: %s', ('http://youtu.be/' + id).yellow.bold)

		server.listen(35000, function () {
			console.log(
				'    %s %s'.bold,
				'NOW PLAYING:'.blue,
				('http://youtu.be/' + id).yellow
			)
			//console.log('[*] Listening http://localhost:35000/')
			//console.log('[*] Starting VLC ...')

			proc.exec('vlc -q --video-on-top --play-and-exit http://localhost:35000/video.mp4', function (err) {
				if (err) {
					console.error(err)
       	        	         	process.exit(-1)
        	       		 }
	        	}).on('exit', function () {
				process.exit(0)
			})

		}).on('error', function () {
			console.error('failed to start server')
			process.exit(-1)
		})

		server.on('request', function (req, res) {
       			 var vid
			pump(ytdl(url, [], {
       		 	    lang: 'en'
	        	}).on('info', function (err, inf) {
				if (!err) {
					console.log('[*] Video info:\n%s', JSON.stringify(inf,null,2))
				}
			}), res, function () {
				//console.log('[*] Stream ended ...')
				process.exit(0);
			})

			//console.log('[*] Streaming ...')
		})
	};

	if (url === 'play' || url === 'lucky') {
		if (url === 'lucky') {
			if (!process.argv[3]) throw 'error: yt lucky <query>';
		} else if (!process.argv[3] || !process.argv[4]) throw 'error: yt play <number> <query>';

                var args = process.argv.slice(url === 'lucky' ? 3 : 4).join(' ')

                search(args, {
                        maxResults: url === 'lucky' ? 1 : parseInt(process.argv[3], 10),
                        startIndex: 1
                }, function (err, results) {
                        if (err) {
                                console.error(err)
                                process.exit(-1)
                        }

                        var res = url === 'lucky' ? results[0] : results[parseInt(process.argv[3], 10) - 1]
                        var qs = querystring.parse(res.url.substr(res.url.indexOf('?') + 1))

                        url = qs.v
			go()
                })
        } else {
		go()
	}
}
