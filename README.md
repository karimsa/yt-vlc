# yt-vlc
watch youtube videos in VLC.

## usage

### finding a video

basic:

```
$ yt search my query
 [1] Video Title - VIDEO_ID
 .. up to 15 results ..
```

example (only first result is shown for sake of brievity):

```
$ yt search everything is awesome
 [1] Everything Is AWESOME!!! -- The LEGO® Movie -- Tegan and Sara feat. The Lonely Island - StTqXEQ2l-Y
 .. up to 15 results ..
```

for results after #15, do:

```
$ yt search START_INDEX my query
 [1] {this would be #15 in a regular search} - VIDEO_ID
 .. up to 15 results ..
```

### playing a video from the search results

play a search result:

```
$ yt search my query
 .. results ..
 [x] video title - VIDEO_ID
 .. results ..
$ yt play x my query
  NOW PLAYING: http://youtu.be/ID
```

### feeling lucky?

```
$ yt lucky my query
  NOW PLAYING: http://youtu.be/ID
```
